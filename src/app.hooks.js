// Application hooks that run for every service

const errorHandler = ctx => {
    if (ctx.error) {
        console.log('error: ', ctx.error)
    }
}

module.exports = {
    before: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    after: {
        all: [],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    },

    error: {
        all: [errorHandler],
        find: [],
        get: [],
        create: [],
        update: [],
        patch: [],
        remove: []
    }
}